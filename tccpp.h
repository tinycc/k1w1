/*
 *  TCC - Tiny C Compiler
 * 
 *  Copyright (c) 2001-2004 Fabrice Bellard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __TCCPP_H__
#define __TCCPP_H__

void save_parse_state(ParseState *s);
void restore_parse_state(ParseState *s);
void preprocess_init(TCCState *s1);
void preprocess_new(void);
char *get_tok_str(int v, CValue *cv);
void next(void);
/* find a token and add it if not found */
TokenSym *tok_alloc(const char *str, int len);
/* label lookup */
Sym *label_find(int v);
Sym *label_push(Sym **ptop, int v, int flags);
/* free define stack until top reaches 'b' */
void free_defines(Sym *b);
void next_nomacro(void);
/* defines handling */
void define_push(int v, int macro_type, int *str, Sym *first_arg);
/* parse after #define */
void parse_define(void);
static inline Sym *define_find(int v)
{
    v -= TOK_IDENT;
    if ((unsigned)v >= (unsigned)(tok_ident - TOK_IDENT))
        return NULL;
    return table_ident[v]->sym_define;
}
void label_pop(Sym **ptop, Sym *slast);
void define_undef(Sym *s);
/* Preprocess the current file */
int tcc_preprocess(TCCState *s1);
/* input with '\[\r]\n' handling. Note that this function cannot
   handle other characters after '\', so you cannot call it inside
   strings or comments */
void minp(void);
/* C comments */
uint8_t *parse_comment(uint8_t *p);
/* return the current character, handling end of block if necessary
   (but not stray) */
int handle_eob(void);
/* read next char from current input file and handle end of input buffer */
static inline void inp(void)
{
    ch = *(++(file->buf_ptr));
    /* end of buffer/file handling */
    if (ch == CH_EOB)
        ch = handle_eob();
}

/* return the number of additional 'ints' necessary to store the
   token */
static inline int tok_ext_size(int t)
{
    switch(t) {
        /* 4 bytes */
    case TOK_CINT:
    case TOK_CUINT:
    case TOK_CCHAR:
    case TOK_LCHAR:
    case TOK_CFLOAT:
    case TOK_LINENUM:
        return 1;
    case TOK_STR:
    case TOK_LSTR:
    case TOK_PPNUM:
        error("unsupported token");
        return 1;
    case TOK_CDOUBLE:
    case TOK_CLLONG:
    case TOK_CULLONG:
        return 2;
    case TOK_CLDOUBLE:
        return LDOUBLE_SIZE / 4;
    default:
        return 0;
    }
}

/* push back current token and set current token to 'last_tok'. Only
   identifier case handled for labels. */
static inline void unget_tok(int last_tok)
{
    int i, n;
    int *q;
    unget_saved_macro_ptr = macro_ptr;
    unget_buffer_enabled = 1;
    q = unget_saved_buffer;
    macro_ptr = q;
    *q++ = tok;
    n = tok_ext_size(tok) - 1;
    for(i=0;i<n;i++)
        *q++ = tokc.tab[i];
    *q = 0; /* end of token string */
    tok = last_tok;
}

static inline void tok_str_new(TokenString *s)
{
    s->str = NULL;
    s->len = 0;
    s->allocated_len = 0;
    s->last_line_num = -1;
}

/* add the current parse token in token string 's' */
void tok_str_add_tok(TokenString *s);
void tok_str_add(TokenString *s, int t);
void tok_str_free(int *str);

#endif /* __TCCPP_H__ */
