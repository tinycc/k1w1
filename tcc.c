/*
 *  TCC - Tiny C Compiler
 * 
 *  Copyright (c) 2001-2004 Fabrice Bellard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "tcc.h"

void help(void)
{
    printf("tcc version " TCC_VERSION " - Tiny C Compiler - Copyright (C) 2001-2006 Fabrice Bellard\n"
           "usage: tcc [-v] [-c] [-o outfile] [-Bdir] [-bench] [-Idir] [-Dsym[=val]] [-Usym]\n"
           "           [-Wwarn] [-g] [-b] [-bt N] [-Ldir] [-llib] [-shared] [-soname name]\n"
           "           [-static] [infile1 infile2...] [-run infile args...]\n"
           "\n"
           "General options:\n"
           "  -v          display current version, increase verbosity\n"
           "  -c          compile only - generate an object file\n"
           "  -o outfile  set output filename\n"
           "  -Bdir       set tcc internal library path\n"
           "  -bench      output compilation statistics\n"
           "  -run        run compiled source\n"
           "  -fflag      set or reset (with 'no-' prefix) 'flag' (see man page)\n"
           "  -Wwarning   set or reset (with 'no-' prefix) 'warning' (see man page)\n"
           "  -w          disable all warnings\n"
           "Preprocessor options:\n"
           "  -E          preprocess only\n"
           "  -Idir       add include path 'dir'\n"
           "  -Dsym[=val] define 'sym' with value 'val'\n"
           "  -Usym       undefine 'sym'\n"
           "Linker options:\n"
           "  -Ldir       add library path 'dir'\n"
           "  -llib       link with dynamic or static library 'lib'\n"
           "  -shared     generate a shared library\n"
           "  -soname     set name for shared library to be used at runtime\n"
           "  -static     static linking\n"
           "  -rdynamic   export all global symbols to dynamic linker\n"
           "  -r          generate (relocatable) object file\n"
           "Debugger options:\n"
           "  -g          generate runtime debug info\n"
#ifdef CONFIG_TCC_BCHECK
           "  -b          compile with built-in memory and bounds checker (implies -g)\n"
#endif
#ifdef CONFIG_TCC_BACKTRACE
           "  -bt N       show N callers in stack traces\n"
#endif
           );
}

int main(int argc, char **argv)
{
    TCCConfig config;
    int i;
    TCCState *s;
    int nb_objfiles, ret, optind;
    char objfilename[1024];
    int64_t start_time = 0;

    s = tcc_new();
    init_tcc_config(&config);

    ret = 0;

    optind = parse_args(&config, s, argc - 1, argv + 1);
    if (config.print_search_dirs) {
        /* enough for Linux kernel */
        printf("install: %s/\n", s->tcc_lib_path);
        return 0;
    }
    if (optind == 0 || config.nb_files == 0) {
        if (optind && s->verbose)
            return 0;
        help();
        return 1;
    }

    nb_objfiles = config.nb_files - config.nb_libraries;

    /* if outfile provided without other options, we output an
       executable */
    if (config.outfile && config.output_type == TCC_OUTPUT_MEMORY)
        config.output_type = TCC_OUTPUT_EXE;

    /* check -c consistency : only single file handled. XXX: checks file type */
    if (config.output_type == TCC_OUTPUT_OBJ && !config.reloc_output) {
        /* accepts only a single input file */
        if (nb_objfiles != 1)
            error("cannot specify multiple files with -c");
        if (config.nb_libraries != 0)
            error("cannot specify libraries with -c");
    }
    

    if (config.output_type == TCC_OUTPUT_PREPROCESS) {
        if (!config.outfile) {
            s->outfile = stdout;
        } else {
            s->outfile = fopen(config.outfile, "w");
            if (!s->outfile)
                error("could not open '%s", config.outfile);
        }
    } else if (config.output_type != TCC_OUTPUT_MEMORY) {
        if (!config.outfile) {
            /* compute default outfile name */
            char *ext;
            const char *name = 
                strcmp(config.files[0], "-") == 0 ? "a" : tcc_basename(config.files[0]);
            pstrcpy(objfilename, sizeof(objfilename), name);
            ext = tcc_fileextension(objfilename);
#ifdef TCC_TARGET_PE
            if (config.output_type == TCC_OUTPUT_DLL)
                strcpy(ext, ".dll");
            else
            if (config.output_type == TCC_OUTPUT_EXE)
                strcpy(ext, ".exe");
            else
#endif
            if (config.output_type == TCC_OUTPUT_OBJ && !config.reloc_output && *ext)
                strcpy(ext, ".o");
            else
                pstrcpy(objfilename, sizeof(objfilename), "a.out");
            config.outfile = objfilename;
        }
    }

    if (config.do_bench) {
        start_time = getclock_us();
    }

    tcc_set_output_type(s, config.output_type);

    /* compile or add each files or library */
    for(i = 0; i < config.nb_files && ret == 0; i++) {
        const char *filename;

        filename = config.files[i];
        if (filename[0] == '-' && filename[1]) {
            if (tcc_add_library(s, filename + 2) < 0) {
                error_noabort("cannot find %s", filename);
                ret = 1;
            }
        } else {
            if (1 == s->verbose)
                printf("-> %s\n", filename);
            if (tcc_add_file(s, filename) < 0)
                ret = 1;
        }
    }

    /* free all files */
    tcc_free(config.files);

    if (0 == ret) {
        if (config.do_bench)
            tcc_print_stats(s, getclock_us() - start_time);

        if (s->output_type == TCC_OUTPUT_PREPROCESS) {
            if (config.outfile)
                fclose(s->outfile);
        } else if (s->output_type == TCC_OUTPUT_MEMORY)
            ret = tcc_run(s, argc - optind, argv + optind);
        else
            ret = tcc_output_file(s, config.outfile) ? 1 : 0;
    }

    tcc_delete(s);

#ifdef MEM_DEBUG
    if (do_bench) {
        printf("memory: %d bytes, max = %d bytes\n", mem_cur_size, mem_max_size);
    }
#endif
    return ret;
}
