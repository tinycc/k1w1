/*
 *  String utility functions for TCC
 * 
 *  Copyright (c) 2001-2004 Fabrice Bellard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __TCCCSTRING_H__
#define __TCCCSTRING_H__

#include "tcc.h"

/* CString handling */
void cstr_realloc(CString *cstr, int new_size);
/* add a byte */
static inline void cstr_ccat(CString *cstr, int ch)
{
    int size;
    size = cstr->size + 1;
    if (size > cstr->size_allocated)
        cstr_realloc(cstr, size);
    ((unsigned char *)cstr->data)[size - 1] = ch;
    cstr->size = size;
}

void cstr_cat(CString *cstr, const char *str);
void cstr_wccat(CString *cstr, int ch);
void cstr_new(CString *cstr);
void cstr_free(CString *cstr);
#define cstr_reset(cstr) cstr_free(cstr)
void add_char(CString *cstr, int c);

#endif /* __TCCCSTRING_H__ */
