/*
 *  TCC - Tiny C Compiler
 * 
 *  Copyright (c) 2001-2004 Fabrice Bellard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __TCCGEN_H__
#define __TCCGEN_H__

void parse_expr_type(CType *type);
void expr_type(CType *type);
void unary_type(CType *type);
void block(int *bsym, int *csym, int *case_sym, int *def_sym,
                  int case_reg, int is_expr);
int expr_const(void);
void expr_eq(void);
void gexpr(void);
void gen_inline_functions(void);
int type_size(CType *type, int *a);
int lvalue_type(int t);
Sym *external_global_sym(int v, CType *type, int r);
void decl(int l);
void decl_initializer(CType *type, Section *sec, unsigned long c,
                             int first, int size_only);
void decl_initializer_alloc(CType *type, AttributeDef *ad, int r, 
                                   int has_init, int v, int scope);
int gv(int rc);
void gv2(int rc1, int rc2);
void move_reg(int r, int s);
void save_regs(int n);
void save_reg(int r);
void vpop(void);
void vswap(void);
void vdup(void);
int get_reg(int rc);
int get_reg_ex(int rc,int rc2);
void mk_pointer(CType *type);
Sym *get_sym_ref(CType *type, Section *sec,
                        unsigned long offset, unsigned long size);
                        
void gen_op(int op);
void force_charshort_cast(int t);
void gen_cast(CType *type);
void vstore(void);

#endif /* __TCCGEN_H__ */
