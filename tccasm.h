/*
 *  GAS like assembler for TCC
 * 
 *  Copyright (c) 2001-2004 Fabrice Bellard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __TCCASM_H__
#define __TCCASM_H__

#ifdef CONFIG_TCC_ASM
void asm_expr(TCCState *s1, ExprValue *pe);
int asm_int_expr(TCCState *s1);
int find_constraint(ASMOperand *operands, int nb_operands,
                           const char *name, const char **pp);
int tcc_assemble(TCCState *s1, int do_preprocess);
void asm_instr(void);
void asm_global_instr(void);
#endif

#endif /* __TCCASM_H__ */
