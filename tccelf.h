/*
 *  ELF file handling for TCC
 * 
 *  Copyright (c) 2001-2004 Fabrice Bellard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __TCCELF_H__
#define __TCCELF_H__

#define ARMAG  "!<arch>\012"    /* For COFF and a.out archives */
#define ELF_PAGE_SIZE  0x1000

typedef struct {
    unsigned int n_strx;         /* index into string table of name */
    unsigned char n_type;         /* type of symbol */
    unsigned char n_other;        /* misc info (usually empty) */
    unsigned short n_desc;        /* description field */
    unsigned int n_value;        /* value of symbol */
} Stab_Sym;

Section *new_symtab(TCCState *s1,
                           const char *symtab_name, int sh_type, int sh_flags,
                           const char *strtab_name, 
                           const char *hash_name, int hash_sh_flags);
/* add tcc runtime libraries */
void tcc_add_runtime(TCCState *s1);
/* relocate symbol table, resolve undefined symbols if do_resolve is
   true and output error if undefined symbol. */
void relocate_syms(TCCState *s1, int do_resolve);
/* relocate common symbols in the .bss section */
void relocate_common_syms(void);
/* relocate a given section (CPU dependent) */
void relocate_section(TCCState *s1, Section *s);
/* add various standard linker symbols (must be done after the
   sections are filled (for example after allocating common
   symbols)) */
void tcc_add_linker_symbols(TCCState *s1);
/* build GOT and PLT entries */
void build_got_entries(TCCState *s1);
/* return elf symbol value or error */
void *tcc_get_symbol_err(TCCState *s, const char *name);
/* load an object file and merge it with current files */
int tcc_load_object_file(TCCState *s1,
                         int fd, unsigned long file_offset);
/* load a '.a' file */
int tcc_load_archive(TCCState *s1, int fd);

/* section generation */
void section_realloc(Section *sec, unsigned long new_size);
void *section_ptr_add(Section *sec, unsigned long size);
void put_extern_sym(Sym *sym, Section *section,
                           unsigned long value, unsigned long size);
void greloc(Section *s, Sym *sym, unsigned long addr, int type);
int put_elf_str(Section *s, const char *sym);
int put_elf_sym(Section *s,
                       unsigned long value, unsigned long size,
                       int info, int other, int shndx, const char *name);
int add_elf_sym(Section *s, uplong value, unsigned long size,
                       int info, int other, int sh_num, const char *name);
void put_elf_reloc(Section *symtab, Section *s, unsigned long offset,
                          int type, int symbol);
void put_stabs(const char *str, int type, int other, int desc,
                      unsigned long value);
void put_stabs_r(const char *str, int type, int other, int desc,
                        unsigned long value, Section *sec, int sym_index);
void put_stabn(int type, int other, int desc, int value);
void put_stabd(int type, int other, int desc);
void *load_data(int fd, unsigned long file_offset, unsigned long size);

#endif /* __TCCELF_H__ */
