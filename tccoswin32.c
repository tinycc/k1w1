/*
 *  Windows specific functions for TCC
 * 
 *  Copyright (c) 2001-2004 Fabrice Bellard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "tcc.h"

/* return the PC at frame level 'level'. Return non zero if not found */
static int rt_get_caller_pc(unsigned long *paddr, 
                            CONTEXT *uc, int level)
{
    unsigned long fp;
    int i;

    if (level == 0) {
        *paddr = uc->Eip;
        return 0;
    } else {          
        fp = uc->Ebp;
        for(i=1;i<level;i++) {
            /* XXX: check address validity with program info */
            if (fp <= 0x1000 || fp >= 0xc0000000)
                return -1;
            fp = ((unsigned long *)fp)[0];
        }
        *paddr = ((unsigned long *)fp)[1];
        return 0;
    }
}

/* emit a run time error at position 'pc' */
static void rt_error(CONTEXT *uc, const char *fmt, ...)
{
    va_list ap;
    unsigned long pc;
    int i;

    va_start(ap, fmt);
    fprintf(stderr, "Runtime error: ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    for(i=0;i<num_callers;i++) {
        if (rt_get_caller_pc(&pc, uc, i) < 0)
            break;
        if (i == 0)
            fprintf(stderr, "at ");
        else
            fprintf(stderr, "by ");
        pc = rt_printline(pc);
        if (pc == rt_prog_main && pc)
            break;
    }
    va_end(ap);
}

static long __stdcall cpu_exception_handler(EXCEPTION_POINTERS *ex_info)
{
    printf("CPU exception: code=0x%8.8x addr=0x%8.8x",
        ex_info->ExceptionRecord->ExceptionCode,
        ex_info->ExceptionRecord->ExceptionAddress);

    rt_error(ex_info->ContextRecord, "error");

    /* TODO: Call longjmp here to return control to user code. */

    return EXCEPTION_CONTINUE_SEARCH;
}

/* Generate a stack backtrace when a CPU exception occurs. */
void handle_cpu_exception(void)
{
    SetUnhandledExceptionFilter(cpu_exception_handler);
}
