#ifndef __I386_ASM_CONFIG_H__
#define __I386_ASM_CONFIG_H__

#define NB_SAVED_REGS 3
#define NB_ASM_REGS 8

void gen_expr32(ExprValue *pe);
void gen_le16(int v);
void asm_opcode(TCCState *s1, int opcode);
void subst_asm_operand(CString *add_str,
                              SValue *sv, int modifier);
void asm_clobber(uint8_t *clobber_regs, const char *str);
void asm_compute_constraints(ASMOperand *operands,
                                    int nb_operands, int nb_outputs,
                                    const uint8_t *clobber_regs,
                                    int *pout_reg);
void asm_gen_code(ASMOperand *operands, int nb_operands,
                         int nb_outputs, int is_output,
                         uint8_t *clobber_regs,
                         int out_reg);

#endif /* __I386_ASM_CONFIG_H__ */

