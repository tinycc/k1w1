/*
 *  Command line argument parsing for TCC
 * 
 *  Copyright (c) 2001-2004 Fabrice Bellard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef __TCCARGS_H__
#define __TCCARGS_H__

typedef struct {
    char **files;
    int nb_files, nb_libraries;
    int multiple_files;
    int print_search_dirs;
    int output_type;
    int reloc_output;
    const char *outfile;
    int do_bench;
} TCCConfig;

void init_tcc_config(TCCConfig *config);
int parse_args(TCCConfig *config, TCCState *s, int argc, char **argv);

#endif /* __TCCARGS_H__ */
